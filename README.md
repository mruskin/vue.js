# Vue.js lifecycle#

* beforeCreate
* created
* beforeMounted (to the DOM)
* mounted (attached to the real DOM, we can see it in a browser)
* beforeUpdate
* updated
* beforeDestroy
* destroyed

# Vue.js COMPUTED#
* Runs only when changes are made